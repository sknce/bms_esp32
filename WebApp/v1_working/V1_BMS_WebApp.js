//TODO ----------- FUKCJA OBSŁUGUJACA ZMIANE KOLORU DIODY ------------------

function diodeStatus(handler, id){
    fetch(handler) // Wysłanie zapytania GET na adres URL
    .then(response => response.text())
    .then(data => {
        var status_diode = document.getElementById(id);

        if(data === "1"){ //! '===' porównanie wartości i typów
            status_diode.style.backgroundColor = "green";
            status_diode.style.boxShadow = "0 0 5px green";
        } else {
            status_diode.style.backgroundColor = "red";
            status_diode.style.boxShadow = "0 0 5px red";
        }
    })
}

//TODO -------- FUNKCJA DO SPRAWDZENIA WARTOSCI PARAMETRU -------------

function parameterValue(handler, id, jednostka){
    fetch(handler)
    .then(response => response.text())
    .then(data => {
        document.getElementById(id).innerHTML = data + jednostka;
    })
}

//? ---------------------------------------------------------------------------

//! ---------------- DIODA STATUSU SYSTEMU -------------------------

// setInterval(diodeStatus('/connectionControl','system_status_diode'), 1000);

// FUNKCJA TESTOWA- MIGANIE DIODY STATUSU
setInterval(function(){
        var status_diode = document.getElementById('system_status_diode');

        
        if(status_diode.style.backgroundColor == "red"){ //! '===' porównanie wartości i typów
            status_diode.style.backgroundColor = "green";
            status_diode.style.boxShadow = "0 0 5px green";
        } else {
            status_diode.style.backgroundColor = "red";
            status_diode.style.boxShadow = "0 0 5px red";
        }
    } ,200);

//! -------------------- PANEL PRAWY ---------------------------

// //? TOTAL CURRENT
// setInterval(parameterValue('/totalCurrent','total_current_value',' A'), 1000);
// //? TOTAL VOLTAGE
// setInterval(parameterValue('/totalVoltage','total_voltage_value',' V'), 1000);
// //? TEMPERATURE I
// setInterval(parameterValue('/temperature1','temperature1_value','&degC'), 1000);
// //? TEMPERATURE II
// setInterval(parameterValue('/temperature2','temperature2_value','&degC'), 1000);

// //! -------------------- PANEL NAD CELAMI ------------------------

// //? BATTERY PERCENTAGE
// setInterval(parameterValue('/voltagePercentage','battery_percentage_value','%'), 1000);
// //? MINIMUM CELL VOLTAGE
// setInterval(parameterValue('/minimumCellVoltage','minimum_cell_voltage_value','V'), 1000);
// //? MAXIMUM CELL VOLTAGE
// setInterval(parameterValue('/maximumCellVoltage','maximum_cell_voltage_value','V'), 1000);

// //! ------------------------- CELLS -------------------------------

// for(var i=1; i<=18; i++){
//     setInterval(parameterValue('/voltageCell'+ i,'voltage_value_cell'+ i,' V'), 1000);
//     setInterval(diodeStatus('/connectionControl_cell'+ i,'diode_cell'+ i), 1000);
// }