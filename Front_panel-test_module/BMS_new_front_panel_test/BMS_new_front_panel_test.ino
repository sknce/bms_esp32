#include <WiFi.h>
#include "index.h"
#include <WebServer.h>
#include <SPI.h>
#include <ADBMS181x.h>
#include <cstring>

// zmienne SPI
bool error = false;
const uint16_t BITRATE = 1000000;

//Konfiguracja serwera
WebServer server(80);
const char* ssid = "BMS_Control_Panel";
const char* password = "1234567890";

void setup() {

  Serial.begin(115200);
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);


  server.on("/", handleRoot);
 
  server.begin();
  Serial.println("Serwer HTTP uruchomiony na adresie: 192.168.4.1 - jest git");

  SPI.begin();                
  SPI.beginTransaction(SPISettings(BITRATE, MSBFIRST, SPI_MODE3));
}

void loop() {
    server.handleClient();
}

  

void handleRoot() {
    server.send(200, "text/html", webpage);
}

