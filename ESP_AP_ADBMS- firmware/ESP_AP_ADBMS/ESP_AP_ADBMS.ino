
#include <WiFi.h>
#include <WebServer.h>
#include <SPI.h>
#include <ADBMS181x.h>
#include <cstring>
#include <math.h>

#define CS 4
#define EXT_LED 33 
#define LED 13
#define OUTPUT_EN 12
#define HallRef 2.536


//zmienne pomocnicze
static unsigned long previousMillis = 0; 
const long interval = 1500;
uint8_t response[6];
uint8_t var;
char buff[64] = {0};

//zmienne do odczytu temperatury
#define R0 4700.0 //rezystor z dzielnika napiecia
#define T0 298.15 // temperatura referencyjna w K
#define T_Beta 3950.0 // wartość B termistora
#define Resolution 65536.0 // rozdzielczosc ADC
#define R_odniesienia 10000.0 // rezystancja w temperaturze odniesienia

//zmienne do obliczania temperatury i rezystancji termistora
float term1 = 20.0;
float term2 = 20.0;
float term3 = 20.0;
float R = 0.0;


// Konfiguracja ADBMS
uint16_t OV = 41000; // max napięcie akumulatora(sum of all cells) - wynika ze wzoru ze strony 30 w dokumentacji ADBMS
uint16_t UV = 30000; // max napięcie akumulatora(sum of all cells) - wynika ze wzoru ze strony 30 w dokumentacji ADBMS
bool REFON = true; // if true reference remains powered up until watchdog timeout
bool ADCOPT = false; // ADC mode option bit
bool FDRF = false; // Force digital redundancy failure
bool DTMEN = true; // Enable discharge timer monitor
bool GPIOBITS_A[5] = {true, true, true, true, true}; // setting gpio as input(true) or output(false)
bool DCCBITS_A[12] = {false, false, false, true, false, false, false, false, false, false, false, false}; 
bool DCTOBITS[4] = {true, false, true, false};
bool GPIOBITS_B[4] = {false, false, false, false}; // setting gpio as input(true) or output(false)
bool DCCBITS_B[7] = {false, false, false, false, false, false, false};
bool PSBITS[2] = {false, false};

// zmienne SPI
bool error = false;
const uint16_t BITRATE = 1000000;

//Konfiguracja serwera
WebServer server(80);
const char* ssid = "BMS_Control_Panel";
const char* password = "1234567890";

//Zmienne wyświetlane na panelu
float voltage = 5.0;
float charge=1.0;
float temp0=1.0;
float temp1=1.0;
float totalVoltage=0.0;
float totalCurrent=1.0;
float cellVoltage[20] = {0};
bool connectionControl = false;


void setup() {

  Serial.begin(115200);
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.on("/", handleRoot);
  server.on("/voltage0", handleVoltage0);
  server.on("/voltage1", handleVoltage1);
  server.on("/voltage2", handleVoltage2);
  server.on("/voltage3", handleVoltage3);
  server.on("/voltage4", handleVoltage4);
  server.on("/voltage5", handleVoltage5);
  server.on("/voltage6", handleVoltage6);
  server.on("/voltage7", handleVoltage7);
  server.on("/voltage8", handleVoltage8);
  server.on("/voltage9", handleVoltage9);
  server.on("/voltage10", handleVoltage10);
  server.on("/voltage11", handleVoltage11);
  server.on("/voltage12", handleVoltage12);
  server.on("/voltage13", handleVoltage13);
  server.on("/voltage14", handleVoltage14);
  server.on("/voltage15", handleVoltage15);
  server.on("/voltage16", handleVoltage16);
  server.on("/voltage17", handleVoltage17);
  server.on("/charge", handleCharge);
  server.on("/connectionControl", handleConnectionControl);
  server.on("/temp0", handleTemp0);
  server.on("/temp1", handleTemp1);
  server.on("/totalVoltage", handleTotalVoltage);
  server.on("/totalCurrent", handleTotalCurrent);
  server.begin();
  Serial.println("Serwer HTTP uruchomiony na adresie: 192.168.4.1");

  pinMode(EXT_LED, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(CS, OUTPUT);  
  digitalWrite(CS, HIGH);
  pinMode(OUTPUT_EN, OUTPUT);  
  digitalWrite(OUTPUT_EN, LOW);

  SPI.begin();                
  SPI.beginTransaction(SPISettings(BITRATE, MSBFIRST, SPI_MODE3));   
  printMenu();
}

void loop() {
    server.handleClient();


  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n'); //czytanie z klawiatury do momentu zmiany linijki
    input.trim(); // usuwa spacje z wpisanego tekstu
    if (input.length() > 0 && isNumeric(input)) { // wchodzimy do petli jeżeli uzytkownik coś wpisal i wpisana wartość to liczba

      var = input.toFloat(); // zmiennia typ zmiennej którą wysłał użytkownik na float

      switch (var) { // wykonanie wybranych przez użytkownika funkcji
        case 0:
          printMenu(); // wypisanie wszytkich funkcji programu --> linijka 442
          break;

        case 1: // zapis i odczyt rejestrów konfigutacyjnych
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */

          /* konfiguracja rejestrów adbms */
          abmsSetConfigA(REFON, ADCOPT, GPIOBITS_A, UV, OV, DCCBITS_A, DCTOBITS, &error); // konfiguracja zmiennych w adbms
          abmsSetConfigB(GPIOBITS_B, DCCBITS_B, DTMEN, PSBITS, FDRF, &error); // konfiguracja zmiennych w adbms
          delay(1000); // konieczne by niektóre z konfiguracji zdążyły zostać wpisane do rejestrów 

          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandReadData(0x00, 0x02, response, 6, &error); //RDCFGA - Read Configuration Register Group A
          sprintf(buff, "CFGA R0:0x%x; R1:0x%x; R2:0x%x; R3:0x%x; R4:0x%x; R5:0x%x", response[0], response[1], response[2], response[3], response[4], response[5]);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x26, response, 6, &error); // RDCFGB - Read Configuration Register Group B
          sprintf(buff, "CFGB R0:0x%x; R1:0x%x; R2:0x%x; R3:0x%x; R4:0x%x; R5:0x%x", response[0], response[1], response[2], response[3], response[4], response[5]);
          Serial.println(buff);
          break;
        case 2:
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandReadData(0x00, 0x02, response, 6, &error);//RDCFGA - Read Configuration Register Group A
          sprintf(buff, "CFGA R0:0x%x; R1:0x%x; R2:0x%x; R3:0x%x; R4:0x%x; R5:0x%x", response[0], response[1], response[2], response[3], response[4], response[5]);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x26, response, 6, &error);// RDCFGB - Read Configuration Register Group B
          sprintf(buff, "CFGB R0:0x%x; R1:0x%x; R2:0x%x; R3:0x%x; R4:0x%x; R5:0x%x", response[0], response[1], response[2], response[3], response[4], response[5]);
          Serial.println(buff);
          break;
        case 3: // odczyt parametrów wewnetrzych - str. 30
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x05, 0x68); //ADSTAT - Start Status Group ADC Conversion and Poll Status
          sendCommandReadData(0x00, 0x10, response, 6, &error); // read status register group A
          sprintf(buff, "STA SC:%.2f; ITMP:%.2f; VA:%.2f",((float)(response[1] << 8 | response[0]))*0.003f, 
                                                           (((float)(response[3] << 8 | response[2]))*0.01316f) - 276, //chyba powinno być 0.01316 - bylo 0.01315
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x12, response, 6, &error); // read status register group B -- bajty 2, 3, 4 to OV/UV celek od 1 do 12
          sprintf(buff, "STB VD:%.2f; R2:0x%x; R3:0x%x; R4:0x%x; R5:0x%x", ((float)(response[1] << 8 | response[0]))/10000.0f, response[2], response[3], response[4], response[5]);
          Serial.println(buff);
          break;
        case 4: // odczyt temperatury i prądu - str. 27
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x05, 0x60); // ADAX - Start GPIOs ADC Conversion and Poll Status
          sendCommandReadData(0x00, 0x0C, response, 6, &error); // Read Auxiliary Register Group A
          sprintf(buff, "Prad BMS I:%.4f; Temperatura T1:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f, 
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x0E, response, 6, &error); // Read Auxiliary Register Group B
          sprintf(buff, "Temperatura T2:%.2f; Temperatura T3:%.2f; Vref:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f, 
                                                           ((float)(response[3] << 8 | response[2]))/10000.0f, 
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          break;
        case 5: // pomiar gpio 1- gpio 9
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x05, 0x60);// ADAX - Start GPIOs ADC Conversion and Poll Status
          sendCommandReadData(0x00, 0x0C, response, 6, &error); // Read Auxiliary Register Group A
          sprintf(buff, "AVA G1:%.2f; G2:%.2f; G3:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f, 
                                                           ((float)(response[3] << 8 | response[2]))/10000.0f, 
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x0E, response, 6, &error);// Read Auxiliary Register Group B
          sprintf(buff, "AVB G4:%.2f; G5:%.2f; Vref:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f, 
                                                           ((float)(response[3] << 8 | response[2]))/10000.0f, 
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x0D, response, 6, &error);// Read Auxiliary Register Group C
          sprintf(buff, "AVC G6:%.2f; G7:%.2f; G8:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f, 
                                                           ((float)(response[3] << 8 | response[2]))/10000.0f, 
                                                           ((float)(response[5] << 8 | response[4]))/10000.0f);
          Serial.println(buff);
          sendCommandReadData(0x00, 0x0F, response, 6, &error);// Read Auxiliary Register Group D -- bajt 4 to OV/UV celek od 13 do 16
          sprintf(buff, "AVD G9:%.2f",((float)(response[1] << 8 | response[0]))/10000.0f);
          Serial.println(buff);

          break;
        case 6:
          digitalWrite(OUTPUT_EN, !digitalRead(OUTPUT_EN));
          
          break;

          /*  Tutaj zaczyna się mój kod - wyświetlane wartości są juz przeliczane  */
        case 7: //odczyt napiec całej celki 
      {
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x03, 0x60); // ADCV - Start Cell Voltage ADC Conversion and Poll Status

          sendCommandReadData(0x00, 0x04, response, 6, &error); //RDCVA -  Read Cell Voltage Register Group A
          float SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4]);
          float SC_before = SC;

          sendCommandReadData(0x00, 0x06, response, 6, &error); //RDCVB -  Read Cell Voltage Register Group B
          SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4])+ SC_before;
          SC_before = SC;

          sendCommandReadData(0x00, 0x08, response, 6, &error); //RDCVC -  Read Cell Voltage Register Group C
          SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4])+ SC_before;
          SC_before = SC;

          sendCommandReadData(0x00, 0x0A, response, 6, &error); //RDCVD -  Read Cell Voltage Register Group D
          SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4]) + SC_before;
          SC_before = SC;

          sendCommandReadData(0x00, 0x09, response, 6, &error); //RDCVE -  Read Cell Voltage Register Group E
          SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4]) + SC_before;
          SC_before = SC;

          sendCommandReadData(0x00, 0x0B, response, 6, &error); //RDCVF -  Read Cell Voltage Register Group F
          SC = (response[1] << 8 | response[0]) + (response[3] << 8 | response[2]) + (response[5] << 8 | response[4]) + SC_before;

          sprintf(buff, "SC: %.4f",SC * 0.0001); //odczyt bajtów z zapisanym napięciem całego pakietu - wzór jest na stronie 65
          Serial.println(buff);
          break;
      }
        case 8: // odczyt napiec pojedynczych celek
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x03, 0x60); // ADCV - Start Cell Voltage ADC Conversion and Poll Status
          sendCommandReadData(0x00, 0x04, response, 6, &error); //RDCVA -  Read Cell Voltage Register Group A
          sprintf(buff, "C1: %.4f, C2: %.4f, C3: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);

          sendCommandReadData(0x00, 0x06, response, 6, &error); //RDCVB -  Read Cell Voltage Register Group B
          sprintf(buff, "C4: %.4f, C5: %.4f, C6: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);
          
          sendCommandReadData(0x00, 0x08, response, 6, &error); //RDCVC -  Read Cell Voltage Register Group C
          sprintf(buff, "C7: %.4f, C8: %.4f, C9: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);
          
          sendCommandReadData(0x00, 0x0A, response, 6, &error); //RDCVD -  Read Cell Voltage Register Group D
          sprintf(buff, "C10: %.4f, C11: %.4f, C12: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);
          
          sendCommandReadData(0x00, 0x09, response, 6, &error); //RDCVE -  Read Cell Voltage Register Group E
          sprintf(buff, "C13: %.4f, C14: %.4f, C15: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);
          
          sendCommandReadData(0x00, 0x0B, response, 6, &error); //RDCVF -  Read Cell Voltage Register Group F
          sprintf(buff, "C16: %.4f, C17: %.4f, C18: %.4f \n",((float)(response[1] << 8 | response[0])) * 0.0001, ((float)(response[3] << 8 | response[2])) * 0.0001, ((float)(response[5] << 8 | response[4])) * 0.0001);
          Serial.println(buff);
          break;
        case 9:
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x05, 0x60); // ADAX - Start GPIOs ADC Conversion and Poll Status
          sendCommandReadData(0x00, 0x0C, response, 6, &error); // Read Auxiliary Register Group A
          sprintf(buff, "Prąd wyjścia: %.4fA",((HallRef - ((float)(response[1] << 8 | response[0])) * 0.0001)) * 50);
          Serial.println(buff);
          break;
        case 10:
        {
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x05, 0x60); // ADAX

          sendCommandReadData(0x00, 0x0C, response, 6, &error); // Read Auxiliary Register Group A
          float T1 = (response[5] << 8 | response[4]) * 0.0001;

          sendCommandReadData(0x00, 0x0E, response, 6, &error); // Read Auxiliary Register Group B
          float T2 = (float)(response[1] << 8 | response[0]) * 0.0001;
          float T3 = (float)(response[3] << 8 | response[2]) * 0.0001;
          float V_ref = (float)(response[5] << 8 | response[4]) * 0.0001;

          R = -1.0 * R0 / (1 - V_ref / T1); // obliczanie rezystancji termistora
          term1 = T0 * T_Beta / (T_Beta + T0 * log(R / R_odniesienia)) - 273.15; // obliczanie temperatury w *C na podstawie rezystancji termistora

          R = -1.0 * R0 / (1 - V_ref / T2); // obliczanie rezystancji termistora
          term2 = T0 * T_Beta / (T_Beta + T0 * log(R / R_odniesienia)) - 273.15; // obliczanie temperatury w *C na podstawie rezystancji termistora

          R = -1.0 * R0 / (1 - V_ref / T3); // obliczanie rezystancji termistora
          term3 = T0 * T_Beta / (T_Beta + T0 * log(R / R_odniesienia)) - 273.15; // obliczanie temperatury w *C na podstawie rezystancji termistora

          sprintf(buff, "T1 = %.4f, T2 = %.4f, T3 = %.4f", term1, term2, term3);
          Serial.println(buff);
        }
          break;
        case 11:{ // odczyt flag UV/OV
          wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
          sendCommandWaitPoll(0x03, 0x60); // ADCV - Start Cell Voltage ADC Conversion and Poll Status

          sendCommandReadData(0x00, 0x12, response, 6, &error); // read status register group B -- zczytywanie flag OVUV z celek 1-12
          uint32_t OVUV1 = response[4] << 16 | response[3] << 8 | response[2];
          
          sprintf(buff, "0x%x", OVUV1);
          Serial.println(buff);
          //zczytywanie każdego bitu i sprawdzanie jaka to flaga
          for(int i = 2; i < 26; i++){

            //sprawdzanie każdego bitu po kolei
            int mask = 1 << (i - 2);
            int masked_UVOV = OVUV1 & mask;
            int this_bit = masked_UVOV >> (i - 2);
            // Serial.println(this_bit);
            
            if(this_bit == 1){
              switch (i % 2){
              case 0:
                sprintf(buff, "C%d ma za małe napięcie", (i/2));
                Serial.println(buff);
              break;
              case 1:
                sprintf(buff, "C%d ma za wysokie napięcie", ((i-1)/2));
                Serial.println(buff);
              break;
            }
            }
          }
          sendCommandReadData(0x00, 0x0F, response, 6, &error);// Read Auxiliary Register Group D -- zczytywanie flag OVUV dla celek 13-18
          uint32_t OVUV2 = response[5] << 8 | response[4];// zapis wartości flag OVUV z celek 13-18 do jednej zmiennej

          for(int i = 2; i < 14; i++){

            //sprawdzanie każdego bitu po kolei
            int mask = 1 << (i - 2);
            int masked_UVOV = OVUV2 & mask;
            int this_bit = masked_UVOV >> (i - 2);
            // Serial.println(this_bit);
            
            if(this_bit == 1){
              switch (i % 2){
              case 0:
                sprintf(buff, "C%d ma za niskie napięcie", ((i+24)/2));
                Serial.println(buff);
              break;
              case 1:
                sprintf(buff, "C%d ma za wysokie napięcie", ((i+23)/2));
                Serial.println(buff);
              break;
            }
            }
          }
        }
        break;     
        default:
          printMenu();
          break;
      }
      memset(buff, 0, sizeof(buff));
    }
  }    
  
  if (millis() - previousMillis >= interval) { // mierzenie 1.5s
    previousMillis = millis();
    connectionControl = !connectionControl;
    digitalWrite(EXT_LED, !digitalRead(EXT_LED));
    wakeUpIC();/* Generic wakeup command to wake the ADBMS181x from sleep state */
    sendCommandReadData(0x00, 0x26, response, 6, &error); // RDCFGB - Read Configuration Register Group B
  }

}

String htmlPage() {
    String html = "<!DOCTYPE html><html><head><title>ESP32 Web Server</title>";
    // Dodanie stylów CSS dla tabeli
     // Istniejące style CSS
    html += "<style>";
    html += "table { border-collapse: collapse; width: 25%; }";
    html += "th, td { border: 1px solid black; text-align: left; padding: 8px; }";
    html += "th { background-color: #f2f2f2; }";
    html += "td { width: 20%; }";
    html += ".green-dot { height: 15px; width: 15px; background-color: #bbb; border-radius: 50%; display: inline-block; }";
    html += ".connected { background-color: #0f0; }"; // Zielony kolor, gdy jest połączenie

    // Dodane style CSS dla kontenerów tabel
    html += ".table-container { display: flex; }";
    html += ".table-cell, .table-battery { margin: 10px; }";
    html += "</style></head><body>";

    // Nagłówek strony
    html += "<h1>BMS DISPLAY PANEL <span id='connectionDot' class='green-dot'></span></h1>";

    // Kontener dla tabel
    html += "<div class='table-container'>";

    // Tabela dla wartości komórek
    html += "<div class='table-cell'>";
    html += "<table>";
    html += "<tr><th>Number</th><th>value</th><th>unit</th></tr>";
    for(int i = 0; i < 18; i++) {
        html += "<tr>";
        html += "<td>CELL " + String(i+1) + "</td>";
        html += "<td id='voltageValue" + String(i) + "'><wartość zmiennej cellVoltage[" + String(i) + "]></td>";
        html += "<td>V</td>";
        html += "</tr>";
    }
    html += "</table>";
    html += "</div>"; 

    // Tabela dla procentu naładowania baterii i reszty scieku 
    html += "<div class='table-battery'>";
    html += "<table>";
    html += "<tr><th>Battery Percentage</th></tr>";
    html += "<tr><td id='batteryPercentage'>--</td></tr>";
    html += "</table>";
   

    // Dodanie odstępu
    html += "<br>";

    // Nowa tabela dla temperatur
    html += "<table>";
    html += "<tr><th>TemperatureSensor</th><th>Value</th><th>unit</th></tr>"; // Nagłówek tabeli
    html += "<tr><td>sensor1</td><td id='tempValue0'>--</td><td>&degC</td></tr>"; // Wiersz dla Temp1
    html += "<tr><td>sensor2</td><td id='tempValue1'>--</td><td>&degC</td></tr>"; // Wiersz dla Temp2
    html += "</table>";
   

    // Dodanie odstępu
    html += "<br>";

    // Nowa tabela dla napiecia
    html += "<table>";
    html += "<tr><th>TotalVoltage</th><th>unit</th></tr>"; // Nagłówek tabeli
    html += "<tr><td id='totalVoltage'>--</td><td>V</td></tr>"; // Wiersz dla VTOTAL
    html += "</table>";
    
    // Dodanie odstępu
    html += "<br>";

 // Nowa tabela dla pradu
    html += "<table>";
    html += "<tr><th>TotalCurrent</th><th>unit</th></tr>"; // Nagłówek tabeli
    html += "<tr><td id='totalCurrent'>--</td><td>A</td></tr>"; // Wiersz dla V1
    html += "</table>";


    html += "</div>"; // Koniec głównego kontenera tabel

    // JavaScript do aktualizacji wartości
    html += "<script>";
    for(int i = 0; i < 18; i++) {
        html += "setInterval(function() {";
        html += "fetch('/voltage" + String(i) + "').then(response => response.text()).then(data => {";
        html += "document.getElementById('voltageValue" + String(i) + "').innerHTML = data;";
        html += "});}, 1000);";
    }
    html += "</script>";

    html += "<script>";
    html += "setInterval(function() {";
    html += "  fetch('/connectionControl').then(response => response.text()).then(data => {"; // Zmiana tutaj, otrzymujemy tekst zamiast JSON
    html += "     var dot = document.getElementById('connectionDot');";
    html += "    if(data === '1') {"; 
    html += "      dot.classList.add('connected');";
    html += "    } else {";
    html += "      dot.classList.remove('connected');";
    html += "    }";
    html += "  });";
    html += "}, 1000);";
    html += "</script>";

    // skrypt na update procentu baterii  
    html += "<script>";
    html += "setInterval(function() {";
    html += "fetch('/charge').then(response => response.text()).then(data => {";
    html += "document.getElementById('batteryPercentage').innerHTML = data;";
    html += "});}, 1000);";
    html += "</script>";
    html += "</body></html>";

//skrypty na temp
    html += "<script>";
    html += "setInterval(function() {";
    html += "fetch('/temp0').then(response => response.text()).then(data => {";
    html += "document.getElementById('tempValue0').innerHTML = data;";
    html += "});}, 1000);";
    html += "</script>";
    html += "</body></html>";

    html += "<script>";
    html += "setInterval(function() {";
    html += "fetch('/temp1').then(response => response.text()).then(data => {";
    html += "document.getElementById('tempValue1').innerHTML = data;";
    html += "});}, 1000);";
    html += "</script>";
    html += "</body></html>";

//skrypt na totalVoltage
    html += "<script>";
    html += "setInterval(function() {";
    html += "fetch('/totalVoltage').then(response => response.text()).then(data => {";
    html += "document.getElementById('totalVoltage').innerHTML = data;";
    html += "});}, 1000);";
    html += "</script>";
    html += "</body></html>";

//skrypt na totalCurrent
    html += "<script>";
    html += "setInterval(function() {";
    html += "fetch('/totalCurrent').then(response => response.text()).then(data => {";
    html += "document.getElementById('totalCurrent').innerHTML = data;";
    html += "});}, 1000);";
    html += "</script>";
    html += "</body></html>";
    return html;
}
void handleRoot() {
    server.send(200, "text/html", htmlPage());
}
void handleCharge() {
    server.send(200, "text/plain", String(charge));
}
void handleTemp0() {
    server.send(200, "text/plain", String(temp0));
}
void handleTemp1() {
    server.send(200, "text/plain", String(temp1));
}
void handleTotalVoltage() {
    server.send(200, "text/plain", String(totalVoltage));
}
void handleTotalCurrent() {
    server.send(200, "text/plain", String(totalCurrent));
}
void handleConnectionControl() {
    server.send(200, "text/plain", String(connectionControl));
}
void handleVoltage0() {
    server.send(200, "text/plain", String(cellVoltage[0]));
}
void handleVoltage1() {
    server.send(200, "text/plain", String(cellVoltage[1]));
}
void handleVoltage2() {
    server.send(200, "text/plain", String(cellVoltage[2]));
}
void handleVoltage3() {
    server.send(200, "text/plain", String(cellVoltage[3]));
}
void handleVoltage4() {
    server.send(200, "text/plain", String(cellVoltage[4]));
}
void handleVoltage5() {
    server.send(200, "text/plain", String(cellVoltage[5]));
}
void handleVoltage6() {
    server.send(200, "text/plain", String(cellVoltage[6]));
}
void handleVoltage7() {
    server.send(200, "text/plain", String(cellVoltage[7]));
}
void handleVoltage8() {
    server.send(200, "text/plain", String(cellVoltage[8]));
}
void handleVoltage9() {
    server.send(200, "text/plain", String(cellVoltage[9]));
}
void handleVoltage10() {
    server.send(200, "text/plain", String(cellVoltage[10]));
}
void handleVoltage11() {
    server.send(200, "text/plain", String(cellVoltage[11]));
}
void handleVoltage12() {
    server.send(200, "text/plain", String(cellVoltage[12]));
}
void handleVoltage13() {
    server.send(200, "text/plain", String(cellVoltage[13]));
}
void handleVoltage14() {
    server.send(200, "text/plain", String(cellVoltage[14]));
}
void handleVoltage15() {
    server.send(200, "text/plain", String(cellVoltage[15]));
}
void handleVoltage16() {
    server.send(200, "text/plain", String(cellVoltage[16]));
}
void handleVoltage17() {
    server.send(200, "text/plain", String(cellVoltage[17]));
}


//sprawdzanie czy znak jest typu numerycznego
bool isNumeric(String str) {
    for (char c : str) {
        if (!isDigit(c) && c != '.' && c != '-') {
            return false;
        }
    }
    return true;
}

void printMenu()
{
  Serial.println();
  Serial.println("0. Menu pomaga wybrać funckję, aby je wyświetlić wybierz 0");
  Serial.println("1. Zapis oraz odczyt konfiguracji rejestrów A i B");
  Serial.println("2. Odczyt konfiguracji rejestrów A i B");
  Serial.println("3. Odczyt parametrów wewnętrznych");
  Serial.println("4. Odczyt temperatur T1, T2 i T3 oraz prądu BMS i napięcia Vref(GPIO: 3 4 5 1; Vref)");
  Serial.println("5. Odczyt napięć wszystkich GPIO");
  Serial.println("6. Otwieranie/Zamykanie kluczy tranzystorowych (output_enable)");
  Serial.println("7. Odczyt napiecia akumulatora");
  Serial.println("8. odczyt napiec pojedynczych celek");
  Serial.println("9. Pomiar prądu wyjścia");
  Serial.println("10. Odczyt temperatury");
  Serial.println("11. Odczyt flag OV UV");
  Serial.println("12. ");
  Serial.println("13. ");
  Serial.println("14. ");
  Serial.println("15. ");
  Serial.println("16. ");
  Serial.println("17. ");
  Serial.println("18. ");
  Serial.println("19. ");
  Serial.println("20. ");
}


/*  wysył danych do ADBMS i odbieranie informacji zwrotnych  */
void sendCommandReadData(uint8_t CMD0, uint8_t CMD1, uint8_t *response, uint8_t response_length, bool *error) { // komenda to 3 ostatnie bity CMD0 i wszystkie bity CMD1

    uint8_t data[4];  
    uint16_t respPec;
    uint16_t respPecCalc;
    data[0] = CMD0;
    data[1] = CMD1;

    uint16_t pec = pec15_calc(2, data);
    data[2] = (uint8_t)(pec >> 8);
    data[3] = (uint8_t)(pec & 0xFF);

    digitalWrite(CS, LOW); 
    for (int i = 0; i < 4; i++) {
        SPI.transfer(data[i]);
    }

    for (int i = 0; i < response_length + 2; i++) {
      if(i < 6) {
        response[i] = SPI.transfer(0x00);
      }else {
        if(i == response_length) {
          respPec = (uint16_t)SPI.transfer(0x00) << 8;
        }else if(i == response_length + 1){
          respPec |= (uint16_t)SPI.transfer(0x00);
        }       
      }
    }
    digitalWrite(CS, HIGH);
    respPecCalc = pec15_calc(6, response);

    if(respPecCalc != respPec) {
      *error = true;
    }
}


/*  wysył informacji bez informacji zwrotnych  */
void sendCommandWaitPoll(uint8_t CMD0, uint8_t CMD1) {
    uint32_t counter = 0; 
    uint16_t value = 0;
    uint8_t dataCMD[4];  
    dataCMD[0] = CMD0;
    dataCMD[1] = CMD1;

    uint16_t pecCMD = pec15_calc(2, dataCMD);
    dataCMD[2] = (uint8_t)(pecCMD >> 8);
    dataCMD[3] = (uint8_t)(pecCMD & 0xFF);

    digitalWrite(CS, LOW); 
    for (int i = 0; i < 4; i++) {
        SPI.transfer(dataCMD[i]);
    }

    while(value == 0 && counter < 200){ 
      counter = counter + 1;
      value = SPI.transfer(0x00);   
    }
    digitalWrite(CS, HIGH);
}


/*  wysyłanie informajci bez informacji o jej dostarczeniu*/
void sendCommand(uint8_t CMD0, uint8_t CMD1) {
    uint32_t counter = 0; 
    uint16_t value = 0;
    uint8_t dataCMD[4];  
    dataCMD[0] = CMD0;
    dataCMD[1] = CMD1;

    uint16_t pecCMD = pec15_calc(2, dataCMD);
    dataCMD[2] = (uint8_t)(pecCMD >> 8);
    dataCMD[3] = (uint8_t)(pecCMD & 0xFF);

    digitalWrite(CS, LOW); 
    for (int i = 0; i < 4; i++) {
        SPI.transfer(dataCMD[i]);
    }
    digitalWrite(CS, HIGH);
}

void sendCommandSendData(uint8_t CMD0, uint8_t CMD1, uint8_t *data, uint8_t dataLen) {

    uint8_t dataCMD[4];  
    dataCMD[0] = CMD0;
    dataCMD[1] = CMD1;

    uint16_t pecCMD = pec15_calc(2, dataCMD);
    dataCMD[2] = (uint8_t)(pecCMD >> 8);
    dataCMD[3] = (uint8_t)(pecCMD & 0xFF);

    uint16_t pec = pec15_calc(dataLen - 2, data);
    data[dataLen - 2] = (uint8_t)(pec >> 8);
    data[dataLen - 1] = (uint8_t)(pec & 0xFF);

    digitalWrite(CS, LOW); 
    for (int i = 0; i < 4; i++) {
        SPI.transfer(dataCMD[i]);
    }
    for (int i = 0; i < dataLen; i++) {
        SPI.transfer(data[i]);
    }
    digitalWrite(CS, HIGH);
}

void uDelay(uint16_t useconds){
  delayMicroseconds(useconds);
}

/* Generic wakeup command to wake the ADBMS181x from sleep state */
void wakeUpIC() 
{
	digitalWrite(CS, LOW);
	uDelay(300); // Guarantees the ADBMS181x will be in standby
	digitalWrite(CS, HIGH);
	uDelay(10);
}

// Funkcje konfiguracyjne Grupa A
void abmsSetConfigA(bool refon, bool adcopt, bool gpio[5], uint16_t uv, uint16_t ov, bool dcc[12], bool dcto[4], bool *error)
{
  uint8_t data[8] = {0};
  
  abmsSetConfigRefon(data, refon);
  abmsSetConfigAdcopt(data, adcopt);
  abmsSetConfigGpioA(data, gpio);
  abmsSetConfigUv(data, uv);
  abmsSetConfigOv(data, ov);
  abmsSetConfigDccA(data, dcc);
  abmsSetConfigDcto(data , dcto);
  sendCommandSendData(0x00, 0x01, data, 8);
}

void abmsSetConfigRefon(uint8_t *data ,bool refon)
{
  if(refon) {
    data[0] |= 0x04; 
  } else {
    data[0] &= 0xFB;
  }
}

void abmsSetConfigAdcopt(uint8_t *data ,bool adcopt)
{
  if(adcopt) {
    data[0] |= 0x01; 
  } else {
    data[0] &= 0xFE;
  }
}

void abmsSetConfigGpioA(uint8_t *data ,bool gpio[5])
{
  for (int i = 0; i < 5; i++)
	{
    if(gpio[i]) {
      data[0] |= (0x01 << (i + 3)); 
    } else {
      data[0] &= (~ (0x01 << (i + 3)));
	  }
  }
}

void abmsSetConfigUv(uint8_t *data ,uint16_t uv)
{
  uint16_t tempUv = (uv / 16);
  data[1] = tempUv & 0xFF; 
  data[2] &= 0xF0; 
  data[2] |= (tempUv >> 8) & 0x0F; 
}

void abmsSetConfigOv(uint8_t *data ,uint16_t ov)
{
  uint16_t tempOv = (ov / 16);
  data[3] = (tempOv >> 4) & 0xFF; 
  data[2] &= 0x0F; 
  data[2] |= (tempOv & 0x000F) << 4;
}

void abmsSetConfigDccA(uint8_t *data ,bool dcc[12])
{
  for (int i = 0; i < 8; i++)
	{
    if(dcc[i]) {
      data[4] |= (0x01 << i); 
    } else {
      data[4] &= (~ (0x01 << i));
	  }
  }
  for (int i = 0; i < 4; i++)
	{
    if(dcc[i + 8]) {
      data[5] |= (0x01 << i); 
    } else {
      data[5] &= (~ (0x01 << i));
	  }
  }  
}

void abmsSetConfigDcto(uint8_t *data ,bool dcto[4])
{
  for (int i = 0; i < 4; i++)
	{
    if(dcto[i]) {
      data[5] |= (0x01 << i + 4); 
    } else {
      data[5] &= (~ (0x01 << i + 4));
	  }
  }  
}

// Funkcje konfiguracyjne Grupa B
void abmsSetConfigB(bool gpio[4], bool dcc[7], bool dtmen, bool ps[2], bool fdrf, bool *error)
{
  uint8_t data[8] = {0};
  
  abmsSetConfigGpioB(data, gpio);
  abmsSetConfigDccB(data, dcc);
  abmsSetConfigDtmen(data, dtmen);
  abmsSetConfigPs(data, ps);
  abmsSetConfigFdrf(data, fdrf);
  sendCommandSendData(0x00, 0x24, data, 8);

}

void abmsSetConfigGpioB(uint8_t *data, bool gpio[4])
{
  for (int i = 0; i < 4; i++)
	{
    if(gpio[i]) {
      data[0] |= (0x01 << i); 
    } else {
      data[0] &= (~ (0x01 << i));
	  }
  }  
}

void abmsSetConfigDtmen(uint8_t *data ,bool dtmen)
{
  if(dtmen) {
    data[1] |= 0x08; 
  } else {
    data[1] &= 0xF7;
  }
}

void abmsSetConfigDccB(uint8_t *data ,bool dcc[2])
{
  if(dcc[0]) {
    data[1] |= 0x04; 
  } else {
    data[1] &= 0xFB;
	}
  for (int i = 1; i < 5; i++)
	{
    if(dcc[i]) {
      data[0] |= (0x01 << i + 3); 
    } else {
      data[0] &= (~ (0x01 << i + 3));
	  }
  } 
  for (int i = 0; i < 2; i++)
	{
    if(dcc[i + 5]) {
      data[1] |= (0x01 << i); 
    } else {
      data[1] &= (~ (0x01 << i));
	  }
  }  
}

void abmsSetConfigPs(uint8_t *data ,bool ps[2])
{
  for (int i = 0; i < 2; i++)
	{
    if(ps[i]) {
      data[1] |= (0x01 << i + 4); 
    } else {
      data[1] &= (~ (0x01 << i + 4));
	  }
  }  
}


void abmsSetConfigFdrf(uint8_t *data ,bool fdrf)
{
  if(fdrf) {
    data[1] |= 0x40; 
  } else {
    data[1] &= 0xBF;
  }
}

